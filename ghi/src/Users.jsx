import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

 const API_HOST = import.meta.env.VITE_API_HOST
 
const Users = () => {
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [newUserEmail, setNewUserEmail] = useState('');
  const [creatingUser, setCreatingUser] = useState(false);

 

  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const response = await fetch(`${API_HOST}/api/users`);
        if (!response.ok) {
          throw new Error('Failed to fetch users');
        }
        const userData = await response.json();
        setUsers(userData);
        setLoading(false);
      } catch (error) {
        setError(error.message);
        setLoading(false);
      }
    };

    fetchUsers();
  }, []);

  const handleNewUserSubmit = async (e) => {
    e.preventDefault();
    setCreatingUser(true);
    try {
      const response = await fetch(`${API_HOST}/api/users`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ email: newUserEmail })
      });
      if (!response.ok) {
        throw new Error('Failed to create user');
      }
      const newUser = await response.json();
      setUsers(prevUsers => [...prevUsers, newUser]);
      setNewUserEmail('');
    } catch (error) {
      setError(error.message);
    } finally {
      setCreatingUser(false);
    }
  };

  return (
    <div>
      <h1>Users</h1>
      <form onSubmit={handleNewUserSubmit}>
        <label>
          New User Email:
          <input
            type="email"
            value={newUserEmail}
            onChange={(e) => setNewUserEmail(e.target.value)}
            required
          />
        </label>
        <button type="submit" disabled={creatingUser}>
          {creatingUser ? 'Creating...' : 'Create User'}
        </button>
      </form>
      {loading ? (
        <p>Loading...</p>
      ) : error ? (
        <p>Error: {error}</p>
      ) : (
        <div>
          <h2>Existing Users:</h2>
          <ul>
            {users.map(user => (
              <li key={user.id}>{user.email}</li>
            ))}
          </ul>
          <nav>
            <ul>
              <li>
                <Link to="/about">About</Link>
              </li>
            </ul>
          </nav>
        </div>
      )}
    </div>
  );
};

export default Users;
